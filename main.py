#!/usr/bin/env python3

import urllib.request, json

with urllib.request.urlopen("https://coronavirus-tracker-api.herokuapp.com/all") as url:
    data = json.loads(url.read().decode())
    #print(data)

#country numbers:
country = (
    "Italy",    #60 it
    "UK",       #61 uk
    "France",   #42 fr
    "Germany",  #54 ge
#    "US",       #47
#    "Iraq",     #84
    #here add other countries from the link above
)

# WORLD STATS
print("World")
print("\tConfirmed:", data["latest"]["confirmed"])
print("\tDeaths:", data["latest"]["deaths"])
print("\tRecovered:", data["latest"]["recovered"])
print()

#COUNTRY STATS
for x in range(84+1):

    if data["deaths"]["locations"][x]["country"] in country:
        print(data["deaths"]["locations"][x]["country"])
        print("\tConfirmed:", data["confirmed"]["locations"][x]["latest"])
        print("\tDeaths:", data["deaths"]["locations"][x]["latest"])
        print("\tRecovered:", data["recovered"]["locations"][x]["latest"])

        if str(data["confirmed"]["locations"][x]["province"]) != "":
            print("\tProvince:", data["confirmed"]["locations"][x]["province"])